#!/bin/bash

docker-compose down
rm -rf data
docker build -t juldap ./juldap/
docker build -t ldap .
docker-compose up