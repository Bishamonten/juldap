<?php

session_start();
?>

<head>
    <link rel="stylesheet" type="text/css" href="css/materialize.min.css" />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
</head>
<body>

<?php

$base_url = "localhost:8080/";
$ldap_host = "ldap://ldap";
$ldap_port = 389;

// si on a les variables de session du id et mdp on se reconnecte
if (is_someone_connected()) {
    $connection = ldap_connect($ldap_host, $ldap_port);

    if ($connection) {
        set_ldap_v3($connection);
        if (is_session_empty()) {
            $current_user = ldap_user($_POST['user']);
                    if (ldap_bind($connection, $current_user, $_POST['pwd'])) {
                        $_SESSION['user'] = $current_user;
                        $_SESSION['pwd'] = $_POST['pwd'];
                    } else {
                        echo my_error('ldap_bind just failed');
                    }
        } else {
            ldap_bind($connection, $_SESSION['user'], $_SESSION['pwd']);
            $current_user = $_SESSION['user'];
        }
    }
}

function is_someone_connected() {
    return (null !== $_SESSION['user'] && null !== $_SESSION['pwd'])
    || (null !== $_POST['user'] && null !== $_POST['pwd']);
}

function is_session_empty() {
    return null === $_SESSION['user'] || null === $_SESSION['pwd'];
}

function ldap_user($user) {
    if ($user == 'admin') {
        return "cn=$user,dc=bla,dc=com";
    } else return 'uid=' . $user . ',cn=admin,dc=bla,dc=com';
}

function my_ldap_connect() {
    return ldap_connect($ldap_host, $ldap_port);
}

function set_ldap_v3($connection) {
    return ldap_set_option($connection, LDAP_OPT_PROTOCOL_VERSION, 3);
}

function my_error($str) {
    return "[ERROR] : " . $str . '<br/>';
}

function jul_dump($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

/*
function format_result_array($result) {
	for ($i = 0; $i < $result['count']; $i++) {
		
	}
}
 */
//function my_ldap_bind

?>

<nav>
    <div class="nav-wrapper pink lighten-1">
        <a href="ldap.php" class="brand-logo">
            <img src="logo.png" height="60" >
        </a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="ldap.php">home</a></li>
            <?php if (is_someone_connected()): ?>
                <li><a href="logout.php">se deconnecter</a></li>
            <?php else: ?>
                <li><a href="login.php">se connecter</a></li>
            <?php endif; ?>
        </ul>
    </div>
</nav>
