<?php

include_once "head.php";
include_once "verifconnection.php";

function is_admin_connection() {
    return 'admin' === $_POST['user'];
}

/*
if (is_admin_connection()) {
	echo "je vois que tu es admin, bg<br/>";
} else {
	echo "connexion standard<br/>";
}
 */
if (!is_someone_connected()) {
    $connection = ldap_connect($ldap_host);
}
if ($connection) {

    if (!set_ldap_v3($connection)) {
        echo my_error("pas possible de mettre en ldapv3");
        die;
    }

    if (!is_someone_connected()) {
        $current_user = ldap_user($_POST['user']);
//var_dump($current_user);

        if (ldap_bind($connection, $current_user, $_POST['pwd'])) {
            $_SESSION['user'] = $current_user;
            $_SESSION['pwd'] = $_POST['pwd'];
            echo "connecté en tant que : $current_user ";
            echo '<a href="' . $base_url  . 'login.php">se deconnecter</a><br/>';
        } else {
            echo my_error('ldap_bind just failed');
        }
    }

    if (isset($_GET['ajouted']) && $_GET['ajouted'] === 'true'): ?>

<div class="container">
    <div class="row">
        <div class="col s12 m6 l6 offset-l3" style="margin-top: 40px">
                <div class="card green">
                    <div class="card-content white-text">
                        <div class="card-title">
                            Ajouté
                        </div>
                    </div>
                </div>
        
        </div>
    </div>
</div>

<?php endif;


// recherche

    // $current_user = 'dc=admin,dc=bla,dc=com'

    // substr($current_user, strpos($current_user, ',') + 1, strlen($current_user) - strpos($current_user, ',')
    $result = ldap_search($connection, "cn=admin,dc=bla,dc=com", "uid=*");
    if ($result) {

        $info = ldap_get_entries($connection, $result);

        ?>
        <div class="container">
            <div class="row">
                <div class="card">
                    <div class="card-content">
                        <div class="card-title">users</div>
                        <table id="person">
                            <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Groupe</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($info as $test) {
                                if ($test['uidnumber'][0] !== null) {
                                    echo '<tr>';
                                    echo '<td>' . $test['givenname'][0] . '</td>';      
                                    echo '<td>' . $test['sn'][0] . '</td>';      
                                    echo '<td>' . $test['uidnumber'][0] . '</td>'; 
                                    echo '<td><a href="http://' . $base_url . 'modifyuser.php?uid=' . $test['uidnumber'][0] . '" class="btn orange white-text">modify</a></td>';
                                    echo '<td><a href="http://' . $base_url . 'deleteuser.php?uid=' . $test['uidnumber'][0] . '" class="btn red white-text">delete</a></td>';
                                    echo '</tr>'; 
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <div class="row right-align" style="margin-top:25px;">
                        <a onclick="saveData()" class="btn cyan white-text right-align">export JSON</a>
                        <script>
                            let json = JSON.stringify(<?php echo json_encode($info); ?>);

                            var saveData = (function () {
                            var a = document.createElement("a");
                            document.body.appendChild(a);
                            a.style = "display: none";
                            return function () {
                                var blob = new Blob([json], {type: "octet/stream"}),
                                    url = window.URL.createObjectURL(blob);
                                a.href = url;
                                a.download = 'users.json';
                                a.click();
                                window.URL.revokeObjectURL(url);
                            };
                                                }());
                        </script>
                        <a href="adduser.php" class="btn cyan white-text">add user</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php

    } else {
        echo my_error('ldap_search just failed');
    }
    $result_group = ldap_search($connection, "cn=admin,dc=bla,dc=com","(objectClass=posixGroup)");
    
    if ($result_group) {

        $info = ldap_get_entries($connection, $result_group);
//echo $info["count"]." entries returned\n";

        ?>
        <div class="container">
            <div class="row">
                <div class="card">
                    <div class="card-content">
                        <div class="card-title">Groupes</div>
                        <table id="person">
                            <thead>
                                <tr>
                                <th>Groupe</th>
                                <th>Type</th>
                                <th>Gid</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($info as $test) {
                                if ($test['gidnumber'][0] !== null) {

                                    echo '<tr>';
                                    echo '<td>' . $test['cn'][0] . '</td>';      
                                    echo '<td>' . $test['objectclass'][0] . '</td>';      
                                    echo '<td>' . $test['gidnumber'][0] . '</td>'; 
                                    echo '<td><a href="http://' . $base_url . 'modifygroup.php?gid=' . $test['gidnumber'][0] . '" class="btn orange white-text">modify</a></td>';
                                    echo '<td><a href="http://' . $base_url . 'deletegroup.php?gid=' . $test['gidnumber'][0] . '" class="btn red white-text">delete</a></td>';
                                    echo '</tr>'; 
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <div class="row right-align" style="margin-top:25px;">
                        <a onclick="saveData()" class="btn cyan white-text right-align">export JSON</a>
                        <script>
                            let json = JSON.stringify(<?php echo json_encode($info); ?>);

                            var saveData = (function () {
                            var a = document.createElement("a");
                            document.body.appendChild(a);
                            a.style = "display: none";
                            return function () {
                                var blob = new Blob([json], {type: "octet/stream"}),
                                    url = window.URL.createObjectURL(blob);
                                a.href = url;
                                a.download = 'users.json';
                                a.click();
                                window.URL.revokeObjectURL(url);
                            };
                                                }());
                        </script>
                        <a href="addgroup.php" class="btn cyan white-text">add group</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php

    } else {
        echo my_error('ldap_search just failed');
    }

} else {
    echo my_error('ldap_connect just failed');
}

//echo ldap_user('test');

// a partir de ce moment la on va faire du ldap youhou !

?>


<?php

include_once 'footer.php';

?>

<script>
    jQuery(document).ready( function () {
        jQuery('#person').DataTable();
    } );
</script>