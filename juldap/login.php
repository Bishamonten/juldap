<?php

include_once "head.php";

if (is_someone_connected()) {
    echo 'deja connecte todo : mettre lien vers panel';
} else {

    ?>

<?php if ($_GET['disconnect'] === 'true'): ?>
<div class="container">
    <div class="row">
        <div class="col s12 m6 l6 offset-l3" style="margin-top: 40px">
            <?php if (!is_someone_connected() && is_session_empty()): ?>
                <div class="card green">
                    <div class="card-content white-text">
                        <div class="card-title">
                            Vous êtes bien déconnecté
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <div class="card red">
                    <div class="card-content white-text">
                        <div class="card-title">
                            Problème de deconnexion
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
            <?php endif; ?>
<?php if ($_GET['connected'] === 'false'): ?>
<div class="container">
    <div class="row">
        <div class="col s12 m6 l6 offset-l3" style="margin-top: 40px">
   
                <div class="card red">
                    <div class="card-content white-text">
                        <div class="card-title">
                            Veuillez vous connecter
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
            <?php endif; ?>


    <div class="container">
        <div class="row">
            <div class="col s12 m6 l6 offset-l3">
                <div class="card center-align">
                    <div class="card-content">
                    <h1 class="card-title">effectuer la connexion</h1>
                        <form action="ldap.php" method="post">
                            <div class="row">
                                <div class="input-field col s6">
                                    <input name="user" id="user" type="text" class="validate" required>
                                    <label for="user">id</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="pwd" name="pwd" type="password" class="validate" required>
                                    <label for="pwd">mdp</label>
                                </div>
                            </div>
                            <div class="card-action">
                            <input class="btn green" type="submit" value="ajouter">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
<?php
include_once "footer.php";
?>