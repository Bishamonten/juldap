<?php

include_once "head.php";
include_once "verifconnection.php";

function is_add_form_ok() {
    if (null === $_POST['groupname'] || $_POST['groupname'] === '') {
        return false;
    }
    return true;
}

if ($connection && is_add_form_ok()) {
    // Connexion avec une identité qui permet les modifications
    $r = ldap_bind($connection, $_SESSION['user'], $_SESSION['pwd']);

    $entries = ldap_get_entries($connection, ldap_search($connection, "dc=bla,dc=com", "gidNumber=*"));
    unset($entries['count']);

    $gids = [1099];
    foreach ($entries as $entry){
        if(isset($entry['gidnumber'])){
            $gids[] = $entry['gidnumber'][0];
        }
    }

    $gidTmp = max($gids) + 1;

    // Prépare les données
    $groupname = $_POST['groupname'];

    $info['objectClass'] = ["top", "posixGroup"];
    $info['description'] = "Groupe:" . $groupname;
    $info['cn'] = $groupname;
    $info['gidNumber'] = $gidTmp;

    $dn = "cn=" . $info['cn'] . ",cn=admin,dc=bla,dc=com";

    $r = ldap_add($connection, $dn, $info);

    ldap_close($connection);
    echo '<script language="Javascript">
           <!--
                 document.location.replace("ldap.php?ajouted=true");
           // -->
     </script>';
    exit();
}

if (!is_add_form_ok()):

    ?>

    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-content">
                    <div class="card-title"><h1>Ajouter Group</h1></div>
                        <form action="addgroup.php" method="post">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input name="groupname" id="group_name" type="text" class="validate">
                                    <label for="group_name">groupname</label>
                                </div>
                            </div>
                            <input class="btn" type="submit" value="ajouter">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php
include_once "footer.php";
?>
