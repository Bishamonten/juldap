<?php

include_once "head.php";

if (isset($_GET['gid'])) {
    $uid_to_delete = $_GET['gid'];

    // @todo : merge deletegroup and deleteuser with a flag 
    $search = ldap_search($connection, "cn=admin,dc=bla,dc=com", "gidNumber=" . $uid_to_delete);
    $people = ldap_get_entries($connection, $search);

    ldap_delete($connection, $people[0]['dn']);

    echo '<script language="Javascript">
           <!--
                 document.location.replace("ldap.php");
           // -->
     </script>';
    exit();
}