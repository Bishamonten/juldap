<?php

include_once "head.php";
include_once "verifconnection.php";

function is_add_form_ok() {
    if (null === $_POST['firstname'] || $_POST['firstname'] === '') {
        return false;
    }
    if (null === $_POST['lastname'] || $_POST['lastname'] === '') {
        return false;
    }
    return true;
}

if ($connection && is_add_form_ok()) {
    // Connexion avec une identité qui permet les modifications
    $r = ldap_bind($connection, $_SESSION['user'], $_SESSION['pwd']);

    $entries = ldap_get_entries($connection, ldap_search($connection, "dc=bla,dc=com", "uidnumber=*"));
    unset($entries['count']);

    $uids = [1099];
    foreach ($entries as $entry){
        if(isset($entry['uidnumber'])){
            $uids[] = $entry['uidnumber'][0];
        }
    }

    $uidTmp = max($uids) + 1;

    // Prépare les données
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];

    $info['objectClass'] = ["top", "person", "organizationalPerson", "inetOrgPerson", "posixAccount", "shadowAccount"];
    $info['givenName'] = $firstname;
    $info["cn"] = $lastname . ' ' . $firstname;
    $info["sn"] = $lastname;
    $info['userPassword'] = 'YmVuZGVy';
    $info['homeDirectory'] = '/home/' . $firstname;
    $info['uidNumber'] = $uidTmp;
    $info['gidNumber'] = $uidTmp;
    $info['loginShell'] = '/bin/bash';
    $info['description'] = 'Utilisateur ' . $firstname . " " . $lastname;
    $info['uid'] = $firstname.'.'.$lastname;

    $r = ldap_add($connection, "cn=" . $info['cn'] . ",cn=admin,dc=bla,dc=com", $info);

    ldap_close($connection);
    echo '<script language="Javascript">
           <!--
                 document.location.replace("ldap.php?ajouted=true");
           // -->
     </script>';
    exit();
}

if (!is_add_form_ok()):

?>

<div class="container">
    <div class="row">
        <div class="card">
            <div class="card-content">
                <div class="card-title"><h1>Ajouter user</h1></div>
                    <form action="adduser.php" method="post">
                        <div class="row">
                            <div class="input-field col s6">
                                <input name="firstname" id="first_name" type="text" class="validate">
                                <label for="first_name">firstname</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="last_name" name="lastname" type="text" class="validate">
                                <label for="last_name">lastname</label>
                            </div>
                        </div>
                        <input class="btn" type="submit" value="ajouter">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php
include_once "footer.php";
?>
