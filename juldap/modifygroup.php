<?php

include_once "head.php";
include_once "verifconnection.php";

function is_add_form_ok() {
    if (null === $_POST['description'] || $_POST['description'] === '') {
        return false;
    }
    if (null === $_POST['dn'] || $_POST['dn'] === '') {
        return false;
    }
    return true;
}

if ($connection && is_add_form_ok()) {
    // Connexion avec une identité qui permet les modifications
    $r = ldap_bind($connection, $_SESSION['user'], $_SESSION['pwd']);
    if(isset($_POST['userGroup'])){
        ldap_mod_replace($connection, $_POST['dn'], ['description' => 'Groupe: ' . $_POST['description'], 'memberUid' => $_POST['userGroup']]);
    } else{
        ldap_mod_replace($connection, $_POST['dn'], ['description' => 'Groupe: ' . $_POST['description']]);
    }
    
    echo '<script language="Javascript">
           <!--
                 document.location.replace("ldap.php?modified=true");
           // -->
     </script>';
    exit();
}

if (!is_add_form_ok()):

if (isset($_GET['gid'])) {
    $gid_to_delete = $_GET['gid'];

    $search = ldap_search($connection, "cn=admin,dc=bla,dc=com", "gidNumber=" . $gid_to_delete);
    $groupes = ldap_get_entries($connection, $search);
    $name = substr($groupes[0]['description'][0], stripos($groupes[0]['description'][0], ":") + 1, strlen($groupes[0]['description'][0]));
    $dn = $groupes[0]['dn'];
    $groupUsers = isset($groupes[0]['memberuid']) ? $groupes[0]['memberuid'] : [] ;
    unset($groupUsers['count']);

    $options = ldap_search($connection, "cn=admin,dc=bla,dc=com", "uid=*");
    $userOptions = ldap_get_entries($connection, $options);
    unset($userOptions['count']);
}

?>

<div class="container">
    <div class="row">
        <div class="card">
            <div class="card-content">
                <div class="card-title"><h1>Edit Group</h1></div>
                    <form action="modifygroup.php" method="post">
                        <div class="row">
                            <div class="input-field col s6">
                                <input name="description" id="first_name" type="text" class="validate" value="<?php echo $name ?>">
                                <label for="description">Description</label>
                            </div>
                            <div class="input-field col s6">
                                <label>Utilisateurs</label>
                                <select multiple name="userGroup[]">
                                    <?php foreach ($userOptions as $user): ?>
                                        <option value="<?= $user['uid'][0]?>"
                                            <?php echo in_array($user['uid'][0], $groupUsers) ? 'selected' : "" ?>>
                                        <?= $user['cn'][0] ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>                
                            </div>
                        </div>
                        <input name="dn" id="dn" value="<?php echo $dn; ?>" class="hide">
                        <input class="btn" type="submit" value="modifier">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>


<?php
include_once "footer.php";
?>
<script type="text/javascript">
    $(document).ready(function() {
        $('select').formSelect();
    });
</script>
