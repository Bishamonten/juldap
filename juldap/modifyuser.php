<?php

include_once "head.php";
include_once "verifconnection.php";

function is_add_form_ok() {
    if (null === $_POST['firstname'] || $_POST['firstname'] === '') {
        return false;
    }
    if (null === $_POST['lastname'] || $_POST['lastname'] === '') {
        return false;
    }
    if (null === $_POST['description'] || $_POST['description'] === '') {
        return false;
    }
    if (null === $_POST['dn'] || $_POST['dn'] === '') {
        return false;
    }
    return true;
}

if ($connection && is_add_form_ok()) {
    // Connexion avec une identité qui permet les modifications
    $r = ldap_bind($connection, $_SESSION['user'], $_SESSION['pwd']);

    if (!ldap_mod_replace($connection, $_POST['dn'], [
        'givenname' => $_POST['firstname'],
        'sn' => $_POST['lastname'],
        'description' => $_POST['description']
    ])) {
        echo "prblm";
    }
    
    echo '<script language="Javascript">
           <!--
                 document.location.replace("ldap.php?modified=true");
           // -->
     </script>';
    exit();
}

if (!is_add_form_ok()):

if (isset($_GET['uid'])) {
    $uid_to_delete = $_GET['uid'];

    $search = ldap_search($connection, "cn=admin,dc=bla,dc=com", "uidNumber=" . $uid_to_delete);
    $people = ldap_get_entries($connection, $search);

    $firstname = $people[0]['givenname'][0];
    $lastname = $people[0]['sn'][0];
    $dn = $people[0]['dn'];
    $description = $people[0]['description'][0];
}

?>

<div class="container">
    <div class="row">
        <div class="card">
            <div class="card-content">
                <div class="card-title"><h1>Modifier user</h1></div>
                    <form action="modifyuser.php" method="post">
                        <div class="row">
                            <div class="input-field col s6">
                                <input name="firstname" id="first_name" type="text" class="validate" value="<?php echo $firstname ?>">
                                <label for="firstname">Prénom</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="last_name" name="lastname" type="text" class="validate" value="<?php echo $lastname ?>">
                                <label for="last_name">Nom</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="description" name="description" type="text" class="validate" value="<?php echo $description ?>">
                                <label for="description">Description</label>
                            </div>
                        </div>
                        <input name="dn" id="dn" value="<?php echo $dn; ?>" class="hide">
                        <input class="btn" type="submit" value="modifier">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>


<?php
include_once "footer.php";
?>
