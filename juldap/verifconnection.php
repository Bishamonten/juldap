<?php

function is_post_login_ok() {
    if (null === $_POST['user'] || $_POST['user'] === '') {
        return false;
    }
    if (null === $_POST['pwd'] || $_POST['pwd'] === '') {
        return false;
    }

    return true;
}

if (!is_someone_connected() && !is_post_login_ok()) {
    echo '<script language="Javascript">
    <!--
    document.location.replace("login.php?connected=false");
    // -->
</script>';
    exit();
}